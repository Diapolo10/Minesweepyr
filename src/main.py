#!python3

import os
import sys
import random
import functools

## CONFIG ##
GRID_WIDTH = 16
GRID_HEIGHT = 8
ERASE_LINE = "\033[K"
CURSOR_UP_ONE = "\033[F"

class Board:
    def __init__(self, x_size: int, y_size: int):
        self.x_size = x_size
        self.y_size = y_size
        self.current_message = None
        self.board = []
        for idx in range(x_size*y_size):
            self.board.append(Square(self, *self.coordinates(idx), has_mine=random.choice([True, False])))

    def __getitem__(self, key):
        return self.board[self.index(*key)]

    def __setitem__(self, key, value):
        self.board[self.index(*key)] = value

    def __str__(self):
        board = []
        for row in range(self.y_size):
            x1 = 0+row*self.x_size
            x2 = self.x_size+row*self.x_size
            board.append("".join(map(str, self.board[x1:x2])))
        return "\n".join(board) + "\n"

    def display(self):
        os.system('cls' if os.name == 'nt' else 'clear')#TODO: Find a better way
        print(self)
        print(self.current_message)

    def coordinates(self, index: int):
        y, x = divmod(max(min(index, self.x_size * self.y_size), 0), self.x_size)
        return (x, y)

    def index(self, x, y):
        return max(min(x, self.x_size), 0) + max(min(y, self.x_size), 0) * self.x_size

    def check_square(self, *coords):
        if self[coords].has_mine:
            state = 4 # BOOM!
            # TODO: End the game
        else:
            state = 1
        self[coords].state = state
        self.current_message = "The square was empty" if state == 1 else "BOOM!"
        return True if state == 4 else False

    def get_number(self, idx):
        num = 0
        squares = []
        x, y = self.coordinates(idx)
        if 0 < x < self.x_size - 1:
            squares.extend([(x-1, y), (x+1, y),])
        if 0 < y < self.y_size - 1:
            squares.extend([(x, y-1), (x, y+1),])
        if len(squares) == 4:
            squares.extend([
                (x-1, y-1),
                (x-1, y+1),
                (x+1, y-1),
                (x+1, y+1)
            ])
        else:
            if x == 0:
                if y == 0:
                    squares.append((x+1, y+1))
                elif y == self.y_size - 1:
                    squares.append((x+1, y-1))
                else:
                    squares.extend([(x+1, y+1), (x+1, y-1)])
            elif x == self.x_size - 1:
                if y == 0:
                    squares.append((x-1, y+1))
                elif y == self.y_size - 1:
                    squares.append((x-1, y-1))
                else:
                    squares.extend([(x-1, y+1), (x-1, y-1)])

            if y == 0:
                if x == 0:
                    squares.append((x+1, y+1))
                elif x == self.x_size - 1:
                    squares.append((x-1, y+1))
                else:
                    squares.extend([(x+1, y+1), (x-1, y+1)])
            elif y == self.y_size - 1:
                if x == 0:
                    squares.append((x+1, y-1))
                elif x == self.x_size - 1:
                    squares.append((x-1, y-1))
                else:
                    squares.extend([(x+1, y-1), (x-1, y-1)])

        for coords in squares:
            if self.board[self.index(*coords)].has_mine:
                num += 1
        return num

class Square:

    STATES = [
        "Unchecked",
        "Empty",
        "Flagged",
        "Unknown",
        "BOOM"
    ]

    def __init__(self, board, *coordinates, has_mine=False):
        self.x, self.y = coordinates
        self.has_mine = has_mine
        self.state = 0
        self.number = 0

    def __str__(self):
        return f"{self.state}"

if __name__ == "__main__":
    b = Board(GRID_WIDTH, GRID_HEIGHT)
    possible_indexes = [i for i in range(b.x_size*b.y_size)]
    random.shuffle(possible_indexes)
    game_over = False
    for idx, i in enumerate(possible_indexes):
        if b.check_square(*b.coordinates(i)):
            game_over = True
        b.display()
        input(f"Round #{idx+1}")
        if game_over:
            break
